const data = [
    {
        name: 'Phones',
        products: [
            {
                name: 'Apple iPhone',
                price: 1500 + '$',
                description: 'Number of processor cores\n' +
                    'Processor clock speed\n' +
                    'RAM '
            },
            {
                name: 'Samsung S22',
                price: 1300 + '$',
                description: 'Number of processor cores\n' +
                    'Processor clock speed\n' +
                    'RAM '
            },
            {
                name: 'Huawei P30',
                price: 1200 + '$',
                description: 'Number of processor cores\n' +
                    'Processor clock speed\n' +
                    'RAM '
            },
        ],
    },
    {
        name: 'Laptops',
        products: [
            {
                name: 'Dell XPS 15',
                price: 2000 + '$',
                description: 'Number of processor cores\n' +
                    'Processor clock speed\n' +
                    'RAM '
            },
            {
                name: 'Apple Macbook Pro',
                price: 3000 + '$',
                description: 'Number of processor cores\n' +
                    'Processor clock speed\n' +
                    'RAM '
            },
        ],
    },
    {
        name: 'Tablets',
        products: [
            {
                name: 'Apple iPad',
                price: 1000 + '$',
                description: 'Number of processor cores\n' +
                    'Processor clock speed\n' +
                    'RAM '
            },
            {
                name: 'Samsung Tab 10',
                price: 1700 + '$',
                description: 'Number of processor cores\n' +
                    'Processor clock speed\n' +
                    'RAM '
            },
        ],
    },
];
