function checkFieldsContent(surname,
                            name,
                            patronymic,
                            city,
                            delivery,
                            payment,
                            amount,
) {
    if (
        surname.length > 0
        && name.length > 0
        && patronymic.length > 0
        && city.length > 0
        && delivery.length > 0
        && payment.length > 0
        && amount.length > 0
    ) {
        return 'checked'

    } else {
        return 'incorrect'
    }

}
