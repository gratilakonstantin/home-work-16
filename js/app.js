const savedLSData = localStorage.getItem('orders')
const savedLSDataObject = JSON.parse(savedLSData);
let orders = savedLSData ? [...savedLSDataObject] : [];

function showCategories() {
    const container = document.querySelector('.categories');

    for (let i = 0; i < data.length; i++) {
        const elem = document.createElement('div');
        elem.textContent = data[i].name;
        const count = i.toString();
        elem.setAttribute('data-category', count);
        elem.addEventListener('click', showProducts);
        container.appendChild(elem);
    }
}

function showProducts(event) {
    const result = document.querySelector('.result');
    const categoryIndex = event.target.getAttribute('data-category');
    const products = data[categoryIndex].products;
    const container = document.querySelector('.products');
    container.innerHTML = '';
    result.innerHTML = '';

    for (let i = 0; i < products.length; i++) {
        const elem = document.createElement('div');
        elem.textContent = products[i].name;
        const count = i.toString()
        elem.setAttribute('data-product', count);
        elem.setAttribute('data-category', categoryIndex);
        elem.addEventListener('click', showDetails);
        container.appendChild(elem);
    }
}

function showDetails(event) {
    const categoryIndex = event.target.getAttribute('data-category');
    const productIndex = event.target.getAttribute('data-product');

    const products = document.querySelector('.products');
    const details = document.querySelector('.details');
    const result = document.querySelector('.result');

    const resultInfo = document.querySelector('.details');
    resultInfo.innerHTML = '';

    const productName = document.createElement('div');
    productName.textContent = data[categoryIndex].products[productIndex].name;


    const productPrice = document.createElement('div');
    productPrice.textContent = data[categoryIndex].products[productIndex].price;


    const productDescription = document.createElement('div');
    productDescription.textContent = data[categoryIndex].products[productIndex].description;


    const submitButton = document.createElement('button');
    submitButton.textContent = 'Bye';

    submitButton.addEventListener('click', showResult);

    function showResult() {
        products.innerHTML = ''
        details.innerHTML = ''
        result.innerHTML = 'Please fill out the form for the delivery of the selected product'
        document.getElementById('block').classList.toggle('hidden');

    }

    resultInfo.appendChild(productName);
    resultInfo.appendChild(productPrice);
    resultInfo.appendChild(productDescription);
    resultInfo.appendChild(submitButton);

    document.getElementById('save').addEventListener('click', function () {
        const surname = document.querySelector('form[name="mainForm"]').elements.surname.value;
        const name = document.querySelector('form[name="mainForm"]').elements.name.value;
        const patronymic = document.querySelector('form[name="mainForm"]').elements.patronymic.value;
        const city = document.querySelector('form[name="mainForm"]').elements.city.value;
        const delivery = document.querySelector('form[name="mainForm"]').elements.delivery.value;
        const payment = document.querySelector('form[name="mainForm"]').elements.payment.value;
        const amount = document.querySelector('form[name="mainForm"]').elements.amount.value;
        const comment = document.querySelector('form[name="mainForm"]').elements.comment.value;

        const checkedData = checkFieldsContent(
            surname,
            name,
            patronymic,
            city,
            delivery,
            payment,
            amount
        );
        const findBiggestId = () => {
            const ids = orders.length !== 0
                ? orders.map(object => {
                    return object.id;
                })
                : [0]
            const maxId = Math.max(...ids);
            return maxId + 1
        };

        const nextId = findBiggestId()

        findBiggestId();

        if (checkedData === "checked") {
            let order = {
                'surname': surname,
                'name': name,
                'patronymic': patronymic,
                'city': city,
                'delivery': delivery,
                'payment': payment,
                'amount': amount,
                'comment': comment,
                'productName': productName.textContent,
                'productPrice': productPrice.textContent,
                'productDescription': productDescription.textContent,
                'id': nextId

            }
            orders.push(order);
            const ordersArrayString = JSON.stringify(orders);
            localStorage.setItem('orders', ordersArrayString);
            const modal = document.querySelector('.modal')
            modal.style.display = 'block';
            modal.innerHTML = `
                <div>
                    <div>order is saved</div>
                    <button class="modalButton">ok</button>
                </div>`;
            document.querySelector('.modalButton').addEventListener('click', function () {
                modal.style.display = 'none';
            })
        } else {
            const modal = document.querySelector('.modal')
            modal.style.display = 'block';
            document.querySelector('.modal').innerHTML = `<div>
                    <div>Fill all fields</div>
                    <button class="modalButton">ok</button>
                </div>`;
            document.querySelector('.modalButton').addEventListener('click', function () {
                modal.style.display = 'none';
            })
        }
        document.querySelector('.modelButton').addEventListener('click', function () {
            document.querySelector('.model').style.display = 'none';
        })

    })
}


document.querySelector('#orders').addEventListener('click', function () {
    const ordersList = localStorage.getItem('orders')
    const ordersListArray = JSON.parse(ordersList)
    const ordersContainer = document.querySelector('.list');
    const containerWrapper = document.querySelector('.container-wrapper');
    const result = document.querySelector('.result');

    const returnButton = document.querySelector('.return')
    returnButton.style.display = 'block'
    returnButton.addEventListener('click', function () {
        returnButton.style.display = 'none'
        ordersContainer.innerHTML = '';
        location.reload();
    })


    ordersContainer.innerHTML = '';
    containerWrapper.innerHTML = '';
    document.getElementById('block').classList.add('hidden');
    result.innerHTML = ''


    function removeListItem(elId) {
        const ordersList = localStorage.getItem('orders')
        const ordersListArray = JSON.parse(ordersList)
        const filteredArray = ordersListArray.filter((item) => {
            return item.id !== elId
        })
        const filteredArrayString = JSON.stringify(filteredArray)
        localStorage.setItem('orders', filteredArrayString);
    }

    for (let i = 0; i <= ordersListArray?.length; i++) {

        const name = document.createElement('div')
        const price = document.createElement('div')
        const date = document.createElement('div')
        const fullData = document.createElement('button')
        fullData.classList.add('item-button')
        fullData.textContent = 'Full data'

        const deleteItem = document.createElement('button')
        deleteItem.classList.add('item-button')
        deleteItem.textContent = 'Delete'
        name.innerHTML = ordersListArray[i].productName
        price.innerHTML = ordersListArray[i].productPrice
        date.innerHTML = new Date().toString()
        ordersContainer.appendChild(name)
        ordersContainer.appendChild(price)
        ordersContainer.appendChild(date)
        ordersContainer.appendChild(fullData)
        ordersContainer.appendChild(deleteItem)


        fullData.addEventListener('click', function () {
            const generalData = document.querySelector('.item-data')
            const customerName = document.

            createElement('div')
            const customerSurname = document.createElement('div')
            const customerPatronymic = document.createElement('div')
            const customerPrice = document.createElement('div')
            generalData.innerHTML = '';
            customerName.innerHTML = ordersListArray[i].name
            customerSurname.innerHTML = ordersListArray[i].surname
            customerPatronymic.innerHTML = ordersListArray[i].patronymic
            customerPrice.innerHTML = ordersListArray[i].productPrice


            generalData.appendChild(customerName)
            generalData.appendChild(customerSurname)
            generalData.appendChild(customerPatronymic)
            generalData.appendChild(customerPrice)

        })
        deleteItem.addEventListener('click', function () {
            const generalDataDelete = document.querySelector('.item-data')
            generalDataDelete.innerHTML = ''
            removeListItem(ordersListArray[i].id)
            ordersContainer.removeChild(name)
            ordersContainer.removeChild(price)
            ordersContainer.removeChild(date)
            ordersContainer.removeChild(fullData)
            ordersContainer.removeChild(deleteItem)
        })
    }
})
showCategories();

